package com.semranurozkaya.projectloodos.base;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.semranurozkaya.projectloodos.ui.activity.main.MainActivity;

public class BaseDialogFragment extends DialogFragment {
    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {

        }
    }
    public void showProgress(){
        if(getActivity()!=null){
            if(getActivity() instanceof MainActivity){
                ((MainActivity)getActivity()).showProgress();
            }
        }
    }

    public void hideProgress(){
        if(getActivity()!=null)
        {
            if(getActivity() instanceof MainActivity){
                ((MainActivity)getActivity()).hideProgress();
            }
        }
    }
}
