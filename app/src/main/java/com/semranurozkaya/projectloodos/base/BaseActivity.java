package com.semranurozkaya.projectloodos.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.semranurozkaya.projectloodos.ui.dialog.AlertDialog;
import com.semranurozkaya.projectloodos.ui.dialog.ProgressDialog;
import com.semranurozkaya.projectloodos.R;

import java.util.Objects;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;
    private Context mContext;
    private Dialog progress;


    public abstract int getContentView();
    public abstract void initView();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getContentView());
        initView();
        mContext = this;

        ButterKnife.bind(this);

    }

    public void showProgress() {

        progress = new Dialog(getContext());
        progress.setContentView(R.layout.progress);
        Objects.requireNonNull(progress.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progress.setCancelable(false);
        progress.show();

    }

    public void hideProgress() {
        if (progress != null && progress.isShowing())
            progress.dismiss();
    }


    public  void showAlert(String message){
        AlertDialog.Companion.newInstance(message).show(getSupportFragmentManager(),AlertDialog.class.getSimpleName());
    }
    public static void  showAlert(Context context, String message){
        AlertDialog.Companion.newInstance(message).show(((BaseActivity)context).getSupportFragmentManager(),AlertDialog.class.getSimpleName()); }

    public Context getContext() {
        return mContext;
    }

    public Activity getActivity() {
        return this;
    }
}

