package com.semranurozkaya.projectloodos.base;

import android.app.Application;

import com.semranurozkaya.projectloodos.utils.TypeFaceUtil;

public class BaseApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //ne amaçla kullandığım anlaşılması açısından yorum satırına aldım
     /*AppHelper.init(getApplicationContext());
     LocalDataManager.init(getApplicationContext());
     */

        TypeFaceUtil.overrideFont(getApplicationContext(), "SERIF", "font/poppinsbold.otf");


    }
}
