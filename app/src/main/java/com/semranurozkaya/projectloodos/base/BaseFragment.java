package com.semranurozkaya.projectloodos.base;

import androidx.fragment.app.Fragment;

import com.semranurozkaya.projectloodos.ui.activity.main.MainActivity;

public class BaseFragment extends Fragment {


    public void showProgress() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).showProgress();
    }

    public void hideProgress() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).hideProgress();
    }

    public void finish() {
        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStack();

    }

    public void openFragmentWithStack(Fragment fragment, String TAG) {
        if (getActivity() != null)
            if (getActivity() instanceof MainActivity)
                ((MainActivity) getActivity()).addFragmentWithStack(fragment, TAG);
    }

}