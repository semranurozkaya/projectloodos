package com.semranurozkaya.projectloodos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SplashTextModel implements Serializable {
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("text_size")
    @Expose
    private Integer textSize;
    @SerializedName("text_color")
    @Expose
    private String textColor;
    @SerializedName("text_duration")
    @Expose
    private Long textDuration;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getTextSize() {
        return textSize;
    }

    public void setTextSize(Integer textSize) {
        this.textSize = textSize;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public Long getTextDuration() {
        return textDuration;
    }

    public void setTextDuration(Long textDuration) {
        this.textDuration = textDuration;
    }
}
