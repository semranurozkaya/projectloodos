package com.semranurozkaya.projectloodos.api;

public interface NetworkResponse<T> {
    void onSuccess(T obj);
    void onError(String message);


}
