package com.semranurozkaya.projectloodos.api;

import android.app.Activity;
import android.content.Context;

public class NetworkRequest {
    private Context mContext;
    private NetworkRequest(Context context) {
        this.mContext = context;
    }

    public static NetworkRequest with(Context context) {
        return new NetworkRequest(context);
    }

    public static NetworkRequest with(Activity activity) {
        return new NetworkRequest(activity);
    }

    public void getFilmList_(String param,NetworkResponse response) {
        NetworkManager.with(mContext)
                .addParams("t",param)
                .callRequest(response);
    }
}
