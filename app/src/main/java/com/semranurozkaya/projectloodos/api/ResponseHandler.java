package com.semranurozkaya.projectloodos.api;

public interface ResponseHandler {
    void onSuccess();
    void onFail(String message);
}
