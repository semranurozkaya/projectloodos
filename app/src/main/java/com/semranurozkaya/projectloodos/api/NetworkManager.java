package com.semranurozkaya.projectloodos.api;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.semranurozkaya.projectloodos.data.Constants;

import java.util.HashMap;
import java.util.Map;


public class NetworkManager<T>  {
    private Context mContex;
    private String method = "POST";
    private Builders.Any.B Ions;
    private String baseUrl = Constants.BASE_URL;
    private String endPoint = "";
    private Map<String, Object> params = new HashMap<>();
    private JsonObject jsonBody = new JsonObject();


    private NetworkManager(Context context) {
        this.mContex = context;
    }

    public static NetworkManager with(Context context) {
        return new NetworkManager(context);
    }

    public static NetworkManager with(Activity activity) {
        return new NetworkManager(activity);
    }

    public static NetworkManager with(Fragment fragment) {
        return new NetworkManager(fragment.getActivity());
    }
    public NetworkManager setJsonBody(JsonObject filterData) {
        this.jsonBody = filterData;
        return this;
    }
    public NetworkManager addParams(String key, String val) {
        params.put(key, val);
        return this;
    }
    public NetworkManager setEndPoint(String endPoint) {
        this.endPoint = endPoint;
        return this;
    }
    @SuppressWarnings("unchecked")
    public void callRequest(final NetworkResponse response){
        String stpost="?"+Constants.KEY_API_KEY+"="+Constants.PARAMETER_API_KEY;

        if (method.equals("POST")) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                String key = entry.getKey();
                String value = (String) entry.getValue();
                //Ions.setBodyParameter(key,value);
                stpost=stpost+"&"+key+"="+value;
            }
            Ions = Ion.with(mContex).load(baseUrl+stpost);
            Ions.addHeader("Content-Type", "application/x-www-form-urlencoded");
            Log.e("Request", baseUrl+stpost);
            stpost="";

            Ions.asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (e != null) {
                        response.onError(e.getMessage());
                        Log.e("Error", e.getMessage());
                    } else {
                        if (result.entrySet().size() > 0) {
                            response.onSuccess(result);
                            Log.e("Response", result.toString());
                        } else
                            Toast.makeText(mContex, "Connection error, please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
