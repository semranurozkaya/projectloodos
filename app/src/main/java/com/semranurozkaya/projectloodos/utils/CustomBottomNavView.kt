package com.semranurozkaya.projectloodos.utils

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.semranurozkaya.projectloodos.R
import com.semranurozkaya.projectloodos.listener.SendBottomMenuListener
import kotlinx.android.synthetic.main.util_bottom_navigation.view.*

class CustomBottomNavView  : FrameLayout {
    var mContext : Context
    var blink : Animation
    var slideUp : Animation
    var slideDown : Animation

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        mContext = context

        val layoutInflater = LayoutInflater.from(context)
        layoutInflater.inflate(R.layout.util_bottom_navigation, this)

        blink = AnimationUtils.loadAnimation(context,R.anim.blink);
        slideUp = AnimationUtils.loadAnimation(context,R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(context,R.anim.slide_down);
        blink.repeatMode = Animation.REVERSE;


        ll_main.background = mContext.resources.getDrawable(R.drawable.shape_bottom_navigation_selected)
        ll_main.startAnimation(slideUp)
        ll_main.isEnabled = false
        clearEnable(ll_main)

        ll_main.setOnClickListener { v ->onClickTab(v)  }
        ll_category.setOnClickListener { v ->onClickTab(v)}
        ll_profile.setOnClickListener { v ->onClickTab(v)}

    }
    fun onClickTab(v:View){
        clearTabs()
        if (v.id == R.id.ll_main) {

            ll_main.isEnabled = false
            clearEnable(ll_main)
            clearAllAnimation()

            ll_main.startAnimation(slideUp)

            ll_main.background = mContext.resources.getDrawable(R.drawable.shape_bottom_navigation_selected)

            val mSendBottomMenuListener = mContext as SendBottomMenuListener
            mSendBottomMenuListener.sendButtomMenu("0")


        } else if (v.id == R.id.ll_category) {

            ll_category.isEnabled = false
            clearEnable(ll_category)
            clearAllAnimation()

            ll_category.startAnimation(slideUp)

            ll_category.background = mContext.resources.getDrawable(R.drawable.shape_bottom_navigation_selected)

            val mSendBottomMenuListener = mContext as SendBottomMenuListener
            mSendBottomMenuListener.sendButtomMenu("1")


        } else if (v.id == R.id.ll_profile) {

            ll_profile.isEnabled = false
            clearEnable(ll_profile)
            clearAllAnimation()

            ll_profile.startAnimation(slideUp)

            ll_profile.background = mContext.resources.getDrawable(R.drawable.shape_bottom_navigation_selected)

            val mSendBottomMenuListener = mContext as SendBottomMenuListener
            mSendBottomMenuListener.sendButtomMenu( "2")


        }
    }

    fun clearTabs(){
        ll_category.setBackgroundResource(0)
        ll_main.setBackgroundResource(0)
        ll_profile.setBackgroundResource(0)
    }
    private fun clearEnable(active: View) {

        if (active === ll_main) {

            ll_category.isEnabled = true
            ll_profile.isEnabled = true

        } else if (active === ll_category) {

            ll_profile.isEnabled = true
            ll_main.isEnabled = true

        } else if (active === ll_profile) {

            ll_main.isEnabled = true
            ll_category.isEnabled = true

        }
    }

    private fun clearAllAnimation() {


        ll_main.clearAnimation()
        ll_profile.clearAnimation()
        ll_category.clearAnimation()
    }

}