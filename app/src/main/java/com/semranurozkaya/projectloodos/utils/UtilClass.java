package com.semranurozkaya.projectloodos.utils;

public class UtilClass {
    public static String remoteResponseAdjustment(String response){
        response=response.replace("{","{ ");
        response=response.replace(":",": ");
        response=response.replace(",",", ");
        response=response.replace("}"," }");

        return response;
    }
}
