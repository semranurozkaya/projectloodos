package com.semranurozkaya.projectloodos.listener;

import com.semranurozkaya.projectloodos.model.FilmsModel;

public interface FilmClickListener {
    void onClickFilm(FilmsModel filmItem);
}
