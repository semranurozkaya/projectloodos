package com.semranurozkaya.projectloodos.ui.fragment.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.semranurozkaya.projectloodos.R;
import com.semranurozkaya.projectloodos.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileFragment extends BaseFragment {
    @BindView(R.id.cl_bg)
    ConstraintLayout cl_bg;

    @BindView(R.id.cl_title_bg)
    ConstraintLayout cl_title_bg;

    @BindView(R.id.pdfView)
    PDFView pdfView;

    @BindView(R.id.tv_title_name)
    TextView tv_title_name;

    @BindView(R.id.searchView)
    SearchView searchView;

    Animation slideUp,slideDown;
    public static ProfileFragment newInstance() {
        
        Bundle args = new Bundle();
        
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideUp = AnimationUtils.loadAnimation(getContext(),R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(getContext(),R.anim.slide_down);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        searchView.setVisibility(View.INVISIBLE);
        tv_title_name.setText(R.string.tv_profile);

        cl_bg.startAnimation(slideUp);
        cl_title_bg.startAnimation(slideDown);

        pdfView.fromAsset("semranurozkaya.pdf")
                .enableSwipe(true) // allows to block changing pages using swipe
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .defaultPage(0)
                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                .password(null)
                .scrollHandle(null)
                .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                // spacing between pages in dp. To define spacing color, set view background
                .spacing(0)
                .pageFitPolicy(FitPolicy.WIDTH)
                .load();
    }

}
