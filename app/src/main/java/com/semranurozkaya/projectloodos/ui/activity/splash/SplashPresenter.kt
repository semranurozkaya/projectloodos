package com.semranurozkaya.projectloodos.ui.activity.splash

import android.util.Log
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import com.semranurozkaya.projectloodos.model.SplashTextModel
import com.semranurozkaya.projectloodos.data.Constants
import com.semranurozkaya.projectloodos.utils.UtilClass

class SplashPresenter(private var mView: SplashContact.View) : SplashContact.Presenter {

    override fun getFirebaseRemoteData() {
        val mFirebaseRemoteConfig: FirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()

        mFirebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(mView.getActivity()!!) { task ->
                    if (task.isSuccessful) {
                        val updated = task.result
                        Log.e("FRC", "Config params updated: $updated")

                        var response = mFirebaseRemoteConfig.getValue(Constants.KEY_FIREBASE_REMOTECONFIG).asString()

                        response = UtilClass.remoteResponseAdjustment(response)
                        val responseJson = JsonParser().parse(response).asJsonObject;
                        val gson = Gson()
                        val element = gson.fromJson<JsonElement>(responseJson.toString(), JsonElement::class.java)
                        val responseItems = gson.fromJson<SplashTextModel>(element, SplashTextModel::class.java)

                        mView.setFirebaseRemoteData(responseItems);

                    } else {

                        Log.e("FRC", "Failed")

                        mView.onErrorFirebaseRemoteConfig("FRC Failed")
                    }
                    //displayWelcomeMessage()
                }
        //routeToAppropriatePage(user)
        //finish()
    }
}