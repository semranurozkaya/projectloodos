package com.semranurozkaya.projectloodos.ui.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.semranurozkaya.projectloodos.R;

public class ProgressDialog extends DialogFragment {

    private static ProgressDialog mProgressDailog;
    TextView tvPercentage;

    public static ProgressDialog getInstance() {

        if (mProgressDailog == null) {
            mProgressDailog = new ProgressDialog();
            mProgressDailog.setCancelable(false);
        }

        return mProgressDailog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {

        View v=inflater.inflate(R.layout.dialog_progress,container);
        initView(v);
        bindEvents();
        return v;

    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        tvPercentage=view.findViewById(R.id.tvPercentage);

    }

    private void bindEvents() {
    }

    private void initView(final View v) {
    }

    public void setVisblePercentage(int visibility){

        if( tvPercentage!=null){
            tvPercentage.setVisibility(visibility);
        }
    }

    public void setercentage(int percentage){

        if( tvPercentage!=null){
            tvPercentage.setText("%"+percentage+"");

        }



    }
}
