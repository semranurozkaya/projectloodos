package com.semranurozkaya.projectloodos.ui.fragment.main;

import android.app.Activity;

import com.semranurozkaya.projectloodos.model.FilmsModel;

import java.util.ArrayList;

public interface MainContact {
    interface Presenter{

        void destroy();

        void resume();

        void pause();

        void getFilm(String filmName);
    }
    interface View{

        Activity getActivity();

        void onError(String errMsg);

        void setFilm(ArrayList<FilmsModel> filmList);

    }
}
