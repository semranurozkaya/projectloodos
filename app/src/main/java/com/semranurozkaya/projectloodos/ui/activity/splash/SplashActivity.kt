package com.semranurozkaya.projectloodos.ui.activity.splash

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Handler
import com.semranurozkaya.projectloodos.base.BaseActivity
import com.semranurozkaya.projectloodos.ui.activity.main.MainActivity
import com.semranurozkaya.projectloodos.R
import com.semranurozkaya.projectloodos.model.SplashTextModel
import com.semranurozkaya.projectloodos.data.Constants
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : BaseActivity() , SplashContact.View {

    private var mRemoteData: SplashTextModel?= null
    private var presenter: SplashContact.Presenter?= null


    override fun getContentView(): Int {
        return R.layout.activity_splash
    }

    override fun initView() {

        if (presenter == null) presenter = SplashPresenter(this)

        presenter!!.getFirebaseRemoteData()
    }

    override fun setFirebaseRemoteData(remoteData: SplashTextModel) {
        this.mRemoteData = remoteData


        if(mRemoteData!=null){
            changeAndWriteText()

            Handler().postDelayed(
                    {
                        routeToAppropriatePage()
                        finishAffinity();
                    },
                    mRemoteData!!.textDuration //Firebase servisinden 4000 geliyor

            )
        }

    }

    private fun changeAndWriteText() {

        tv_splash_anim.setText("");
        tv_splash_anim.textSize = mRemoteData!!.textSize.toFloat()
        tv_splash_anim.setTextColor(Color.parseColor("#"+mRemoteData!!.textColor))
        tv_splash_anim.setCharacterDelay(150);
        tv_splash_anim.animateText(mRemoteData!!.text);

    }

    override fun onErrorFirebaseRemoteConfig(errMsg: String) {
        showAlert(errMsg)

    }

    private fun routeToAppropriatePage() {
        when {
            //mRemoteData == null -> LoginActivity.start(this)  //servisten veri çekilemediyse login ekranına gönderilebilir uygulamaya göre degisiklik gösterir

            mRemoteData!!.text.length>0 -> startActivity(Intent(this, MainActivity::class.java)) // koşullar sağlıyor mu kontrol edilir >0 rastgele bir örnekti
            //else -> LoginActivity.start(this)  koşullar sağlamıyorsa örnegin uygulamanın login ekranı varsa oraya yönlendirilebilir
        }
    }

    private fun getSplashScreenDuration(): Long {
        val sp = getPreferences(Context.MODE_PRIVATE)

        return when (sp.getBoolean(Constants.KEY_FIREBASE_REMOTECONFIG, true)) {
            true -> {
                //ilk açılış
                sp.edit().putBoolean(Constants.KEY_FIREBASE_REMOTECONFIG, false).apply()
                mRemoteData!!.textDuration
            }
            false -> {
                //daha önce açıldıysa
                1000
            }
        }
    }
}