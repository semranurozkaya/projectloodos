package com.semranurozkaya.projectloodos.ui.fragment.main;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.semranurozkaya.projectloodos.R;
import com.semranurozkaya.projectloodos.api.NetworkRequest;
import com.semranurozkaya.projectloodos.api.NetworkResponse;
import com.semranurozkaya.projectloodos.data.Constants;
import com.semranurozkaya.projectloodos.model.FilmsModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainPresenter implements MainContact.Presenter{
    private NetworkRequest mNetworkRequest;
    private MainContact.View mView;


    public MainPresenter( MainContact.View mView) {

        this.mView = mView;

    }

    @Override
    public void destroy() {
        mView=null;
    }

    @Override
    public void resume() {
        if(mView!=null){

            //onResume olduğu her an çağıracağım fonksiyonu yazıyorum bu sayfa için gerekmiyor

        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void getFilm(String filmName) {
        getFilmList(filmName);
    }

    public void getFilmList(String filmname){
        NetworkRequest.with(mView.getActivity()).getFilmList_(filmname,new NetworkResponse() {
            @Override
            public void onSuccess(Object obj) {
             JsonObject jsonObject = (JsonObject) obj;
                if(jsonObject.has(Constants.KEY_RESPONSE) && jsonObject.get(Constants.KEY_RESPONSE).getAsBoolean()){

                   /* // Eğer response array olarak gelseydi
                    JsonArray arr = o.getAsJsonArray(Constants.KEY_RESULT).getAsJsonArray();
                    Type listType = new TypeToken<List<FilmsModel>>(){}.getType();
                    List<FilmsModel> filmList = new Gson().fromJson(arr,listType);*/


                    FilmsModel filmsItem = new Gson().fromJson(jsonObject,FilmsModel.class);

                    //gelen response array olarak gelmediği için ve benim de liste yapısını gösterebilmem için array'e attım
                    ArrayList<FilmsModel> filmList = new ArrayList<FilmsModel>();
                    filmList.add(filmsItem);

                    mView.setFilm(filmList);


                }
                else if(jsonObject.has(Constants.KEY_ERROR)){
                    String ergMsg=jsonObject.get(Constants.KEY_ERROR).getAsString();
                    mView.onError(ergMsg);
                }
                else mView.onError(mView.getActivity().getString(R.string.message_api_error));
            }

            @Override
            public void onError(String message) {

                mView.onError(message);
            }
        });
    }
}
