package com.semranurozkaya.projectloodos.ui.fragment.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.semranurozkaya.projectloodos.R;
import com.semranurozkaya.projectloodos.listener.FilmClickListener;
import com.semranurozkaya.projectloodos.model.FilmsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FilmAdapter extends RecyclerView.Adapter<FilmAdapter.MyViewHolder>{

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<FilmsModel> item;
    FilmClickListener listener;

    public FilmAdapter(Activity activity, ArrayList<FilmsModel> item, FilmClickListener listener) {
        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.item=item;
        this.listener=listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_film, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_name.setText(item.get(position).getTitle());
        holder.tv_language.setText(item.get(position).getLanguage());
        holder.tv_released.setText(item.get(position).getReleased());

        try{
            holder.tv_imdb.setText(item.get(position).getRatings().get(0).getValue());
            String startemp =item.get(position).getRatings().get(0).getValue();
            startemp=startemp.substring(0,startemp.indexOf('/'));
            holder.ratingBar.setRating((float) (Float.parseFloat(startemp)/2.0));
        }catch (Exception e){
            holder.tv_imdb.setText("");

            holder.ratingBar.setRating(0);
        }
        Picasso.get()
                .load(item.get(position).getPoster())
                .error(android.R.drawable.stat_notify_error)
                .placeholder( R.drawable.progress )
                .into(holder.iv_film);

        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickFilm(item.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_name;
        TextView tv_language;
        TextView tv_released;
        TextView tv_imdb;
        RatingBar ratingBar;
        ImageView iv_film;
        LinearLayout ll_item;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_language = itemView.findViewById(R.id.tv_language);
            tv_released = itemView.findViewById(R.id.tv_released);
            tv_imdb = itemView.findViewById(R.id.tv_imdb);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            iv_film = itemView.findViewById(R.id.iv_film);
            ll_item = itemView.findViewById(R.id.ll_item);
        }
    }

}
