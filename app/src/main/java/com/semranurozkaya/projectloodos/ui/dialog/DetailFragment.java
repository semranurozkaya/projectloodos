package com.semranurozkaya.projectloodos.ui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.semranurozkaya.projectloodos.R;
import com.semranurozkaya.projectloodos.base.BaseDialogFragment;
import com.semranurozkaya.projectloodos.model.FilmsModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailFragment extends BaseDialogFragment {

    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_x)
    TextView tv_x;
    @BindView(R.id.tv_year)
    TextView tv_year;
    @BindView(R.id.tv_gendre)
    TextView tv_gendre;
    @BindView(R.id.tv_detail)
    TextView tv_detail;
    @BindView(R.id.iv_poster)
    ImageView iv_film;
    private FilmsModel filmDetail;

    public static DetailFragment newInstance(FilmsModel filmDetail) {

        Bundle args = new Bundle();

        DetailFragment fragment = new DetailFragment();
        fragment.filmDetail=filmDetail;
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
    }


    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.dimAmount = .6f;
        params.flags|= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(params);
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.dialog_film_detail,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this,view);
        tv_name.setText(filmDetail.getTitle());
        tv_gendre.setText(filmDetail.getGenre());
        tv_detail.setText(filmDetail.getPlot());
        tv_year.setText("("+filmDetail.getYear()+") "+filmDetail.getRuntime());
        Picasso.get()
                .load(filmDetail.getPoster())
                .error(android.R.drawable.stat_notify_error)
                .placeholder( R.drawable.progress )
                .into(iv_film);


        tv_x.setOnClickListener(v -> dismiss());
    }
}
