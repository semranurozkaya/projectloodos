package com.semranurozkaya.projectloodos.ui.fragment.category;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.semranurozkaya.projectloodos.R;
import com.semranurozkaya.projectloodos.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryFragment extends BaseFragment {
    @BindView(R.id.cl_bg)
    ConstraintLayout cl_bg;

    @BindView(R.id.cl_title_bg)
    ConstraintLayout cl_title_bg;

    @BindView(R.id.tv_title_name)
    TextView tv_title_name;

    @BindView(R.id.searchView)
    SearchView searchView;

    Animation slideUp,slideDown;
    public static CategoryFragment newInstance() {

        Bundle args = new Bundle();

        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_category, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideUp = AnimationUtils.loadAnimation(getContext(),R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(getContext(),R.anim.slide_down);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        cl_bg.startAnimation(slideUp);
        cl_title_bg.startAnimation(slideDown);

        tv_title_name.setText(R.string.tv_category);
        searchView.setVisibility(View.INVISIBLE);


    }
}
