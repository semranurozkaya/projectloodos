package com.semranurozkaya.projectloodos.ui.fragment.main;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.semranurozkaya.projectloodos.R;
import com.semranurozkaya.projectloodos.base.BaseActivity;
import com.semranurozkaya.projectloodos.base.BaseFragment;
import com.semranurozkaya.projectloodos.listener.FilmClickListener;
import com.semranurozkaya.projectloodos.model.FilmsModel;
import com.semranurozkaya.projectloodos.ui.dialog.DetailFragment;
import com.semranurozkaya.projectloodos.ui.fragment.adapter.FilmAdapter;

import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainFragment extends BaseFragment implements MainContact.View, FilmClickListener {

    @BindView(R.id.cl_bg)
    ConstraintLayout cl_bg;

    @BindView(R.id.cl_title_bg)
    ConstraintLayout cl_title_bg;

    @BindView(R.id.rv_film)
    RecyclerView rv_film;

    @BindView(R.id.srl_film)
    SwipeRefreshLayout srl_film;

    @BindView(R.id.tv_title_name)
    TextView tv_title_name;

    @BindView(R.id.searchView)
    SearchView searchView;

    @BindView(R.id.cl_progress)
    ConstraintLayout cl_progress;

    private MainContact.Presenter mMainPresenter;
    private String searcQuery="";
    Animation slideUp,slideDown,fadeIn,fadeOut;
    ArrayList<FilmsModel> filmList;
    FilmAdapter adapter;
    ConstraintLayout.LayoutParams params;


    public static MainFragment newInstance() {
        
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        params = (ConstraintLayout.LayoutParams)tv_title_name.getLayoutParams();

        cl_bg.startAnimation(slideUp);
        cl_title_bg.startAnimation(slideDown);

        tv_title_name.setText(R.string.tv_title_main);

        srl_film.setEnabled(false);

        srl_film.setOnRefreshListener(() -> {

            if (!searcQuery.equals(""))
                cl_progress.setVisibility(View.VISIBLE);
                mMainPresenter.getFilm(searcQuery);

        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                cl_progress.setVisibility(View.VISIBLE);
                mMainPresenter.getFilm(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searcQuery = newText;
                return false;
            }
        });
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                tv_title_name.startAnimation(fadeOut);
            }
        });

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(searchView.hasFocus()){

                    params.verticalBias = 0.2f;
                    tv_title_name.setLayoutParams(params);

                    tv_title_name.startAnimation(fadeIn);
                }
                else {

                    params.verticalBias = 0.65f;
                    tv_title_name.setLayoutParams(params);

                    tv_title_name.startAnimation(fadeIn);

                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(mMainPresenter == null) mMainPresenter = new MainPresenter(this);
        slideUp = AnimationUtils.loadAnimation(getContext(),R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(getContext(),R.anim.slide_down);
        fadeIn = AnimationUtils.loadAnimation(getContext(),R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(getContext(),R.anim.fade_out);

    }

    @Override
    public void onError(String errMsg) {
        cl_progress.setVisibility(View.GONE);
        BaseActivity.showAlert(getContext(),errMsg);
    }

    @Override
    public void setFilm(ArrayList<FilmsModel> filmList) {
        cl_progress.setVisibility(View.GONE);
        this.filmList=filmList;

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rv_film.setLayoutManager(linearLayoutManager);
        adapter=new FilmAdapter(getActivity(),filmList,this);
        rv_film.setAdapter(adapter);

        srl_film.setRefreshing(false);
        srl_film.setEnabled(true);

    }

    @Override
    public void onClickFilm(FilmsModel filmItem) {
        DetailFragment.newInstance(filmItem).show(getActivity().getSupportFragmentManager(),DetailFragment.class.getSimpleName());
    }

}
