package com.semranurozkaya.projectloodos.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.semranurozkaya.projectloodos.R
import com.semranurozkaya.projectloodos.listener.DialogConfirmListener
import kotlinx.android.synthetic.main.dialog_alert.*

class AlertDialog : DialogFragment() {
    var dialogConfirmListener: DialogConfirmListener? = null
    var message: String=""

    companion object{
        fun newInstance(message:String?): AlertDialog {
            val args = Bundle()
            val fragment = AlertDialog()
            fragment.message= message!!
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
                DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar
        )
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_alert,container,false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_message.setText(message)

        btn_ok.setOnClickListener {
            dismiss()
        }

    }

    override fun onStart() {
        super.onStart()
        val window = dialog!!.window
        val windowParams = window!!.attributes
        windowParams.dimAmount = 0.60f
        windowParams.flags = windowParams.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
        window.attributes = windowParams
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        //getDialog().setCancelable(false);
    }

    fun setConfirmListener(confirmListener: DialogConfirmListener): AlertDialog {
        this.dialogConfirmListener = confirmListener
        return this
    }
}