package com.semranurozkaya.projectloodos.ui.activity.main;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;
import com.semranurozkaya.projectloodos.base.BaseActivity;
import com.semranurozkaya.projectloodos.R;
import com.semranurozkaya.projectloodos.listener.SendBottomMenuListener;
import com.semranurozkaya.projectloodos.ui.fragment.category.CategoryFragment;
import com.semranurozkaya.projectloodos.ui.fragment.main.MainFragment;
import com.semranurozkaya.projectloodos.ui.fragment.profile.ProfileFragment;
import com.semranurozkaya.projectloodos.utils.CustomBottomNavView;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements SendBottomMenuListener {

    private CustomBottomNavView mCustomBottomNavigationView;
    int container = R.id.fl_container;
    Animation slideUp;

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.ll_profile)
    LinearLayout ll_profile;
    @BindView(R.id.ll_category)
    LinearLayout ll_category;
    @BindView(R.id.ll_main)
    LinearLayout ll_main;
    @BindView(R.id.fl_container)
    FrameLayout fl_container;
    private boolean doubleBackToExitPressedOnce=false;
    private Snackbar snackbar;

    @Override
    public int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    public void initView() {
        mCustomBottomNavigationView=findViewById(R.id.mNavigaitonView);
        slideUp = AnimationUtils.loadAnimation(getActivity(),R.anim.slide_up);
        loadMainFragment();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSnackBar();

    }

    @Override
    public void sendButtomMenu(String id) {

        switch (id) {
            case "0":
                addFragmentWithStack(MainFragment.newInstance(),MainFragment.class.getSimpleName());
                break;
            case "1":
                addFragmentWithStack(CategoryFragment.newInstance(),CategoryFragment.class.getSimpleName());
                break;
            case "2":
                addFragmentWithStack(ProfileFragment.newInstance(),ProfileFragment.class.getSimpleName());
                break;
        }
    }

    public void addFragmentWithoutStack(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(container, fragment).commitAllowingStateLoss();

    }

    public void addFragmentWithStack(Fragment fragment, String Tag) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        //fm.popBackStack();
        ft.addToBackStack(Tag);
        ft.add(container, fragment).commitAllowingStateLoss();

    }

    public void loadMainFragment() {
        addFragmentWithoutStack(MainFragment.newInstance());
    }

    public void initSnackBar() {

        snackbar = Snackbar
                .make(mCoordinatorLayout, getString(R.string.snackbar_message), Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));


    }

    public Fragment currentFragment() {
        return getSupportFragmentManager().findFragmentById(container);
    }
    @Override
    public void onBackPressed() {

        super.onBackPressed();
        mCustomBottomNavigationView.clearTabs();

        if(currentFragment() instanceof ProfileFragment){
            ll_profile.setEnabled(false);
            clearEnable(ll_profile);
            ll_profile.setBackground(getContext().getResources().getDrawable(R.drawable.shape_bottom_navigation_selected));
        }else if(currentFragment() instanceof CategoryFragment){
            ll_category.setEnabled(false);
            clearEnable(ll_category);
            ll_category.setBackground(getContext().getResources().getDrawable(R.drawable.shape_bottom_navigation_selected));
        }else if(currentFragment() instanceof MainFragment){
            ll_main.setEnabled(false);
            clearEnable(ll_main);
            ll_main.setBackground(getContext().getResources().getDrawable(R.drawable.shape_bottom_navigation_selected));
            /*if(doubleBackToExitPressedOnce){
                snackbar.dismiss();
                finish();

            }else{
                doubleBackToExitPressedOnce=true;
                snackbar.show();

            }*/
        }

    }
    private void clearEnable(View active) {

        if (active == ll_main) {

            ll_category.setEnabled(true);
            ll_profile.setEnabled(true);

        } else if (active == ll_category) {

            ll_profile.setEnabled(true);
            ll_main.setEnabled(true);

        } else if (active == ll_profile) {

            ll_main.setEnabled(true);
            ll_category.setEnabled(true);

        }
    }

}
