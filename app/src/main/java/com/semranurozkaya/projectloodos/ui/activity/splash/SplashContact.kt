package com.semranurozkaya.projectloodos.ui.activity.splash

import android.app.Activity
import com.semranurozkaya.projectloodos.model.SplashTextModel

interface SplashContact {

    interface Presenter{

        fun getFirebaseRemoteData()

    }

    interface View{

        fun getActivity(): Activity?
        fun setFirebaseRemoteData(remoteData: SplashTextModel)
        fun onErrorFirebaseRemoteConfig(errMsg:String)
    }
}