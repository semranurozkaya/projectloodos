package com.semranurozkaya.projectloodos.data;

public class Constants {
    public static final String KEY_FIREBASE_REMOTECONFIG = "loodos_text_info";
    public static final String BASE_URL = "http://omdbapi.com/";
    public static final String KEY_API_KEY ="apikey";
    public static final String PARAMETER_API_KEY="7d8abe8c";
    public static final String KEY_T="t";
    public static final String PARAMETER_EXAMPLES="Examples";

    public static final String KEY_RESPONSE ="Response";
    public static final String KEY_ERROR ="Error";

}
